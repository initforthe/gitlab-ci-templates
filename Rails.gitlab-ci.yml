# Auto DevOps for Rails
# This CI/CD configuration provides a standard pipeline for
# * building a Docker image,
# * storing the image in the container registry,
# * running tests from the Docker image,
# * running code quality analysis,
# * creating a review app for each topic branch,
# * and continuous deployment to production
#
# Test jobs may be disabled by setting environment variables:
# * test: TEST_DISABLED
# * code_quality: CODE_QUALITY_DISABLED
# * review: REVIEW_DISABLED
# * stop_review: REVIEW_DISABLED
#
# Continuous deployment to production is enabled by default.
# If you want to deploy to staging first, set STAGING_ENABLED environment variable.

image: alpine:latest

variables:
  BUNDLE_APP_CONFIG: ${CI_PROJECT_DIR}/.bundle
  DATABASE_HOST: postgres
  DATABASE_PORT: '5432'
  DATABASE_USER: postgres
  DATABASE_PASSWORD: postgres-password
  DATABASE_NAME: ${CI_ENVIRONMENT_SLUG}
  DATABASE_IMAGE: postgres
  DATABASE_VERSION: '10'
  DATABASE_SCHEME: postgres

  POSTGRES_USER: ${DATABASE_USER}
  POSTGRES_PASSWORD: ${DATABASE_PASSWORD}

  NODE_VERSION: '11'
  RUBY_VERSION: '2.5'
  BUILDER_VERSION: 2-5-stable

  DOCKER_DRIVER: overlay2

  DOCKER_TLS_CERTDIR: "" # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501

  SCHEMA_TYPE: schema # or structure

  BUNDLE_BUILD__SASSC: "--disable-march-tune-native" # https://github.com/sass/sassc-ruby/issues/146#issuecomment-536800427

cache:
  paths:
    - .bundle
    - vendor/bundle
    - node_modules
    - public/assets
    - public/packs
    - tmp/*.log

stages:
  - compile
  - test
  - build
  - deploy # dummy stage to follow the template guidelines
  - review
  # - dast
  - staging
  - production
  # - performance
  - cleanup

include:
  - project: initforthe/gitlab-ci-templates
    file: /Jobs/Compile.gitlab-ci.yml
  - project: initforthe/gitlab-ci-templates
    file: /Jobs/Test.gitlab-ci.yml
  - project: initforthe/gitlab-ci-templates
    file: /Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Build.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml
  - project: initforthe/gitlab-ci-templates
    file: /Jobs/Deploy.gitlab-ci.yml

  # - template: Jobs/Build.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml
  # - template: Jobs/Test.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml
  # - template: Jobs/Code-Quality.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  # - template: Jobs/Deploy.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml
  # - template: Jobs/Browser-Performance-Testing.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml
  # - template: Security/DAST.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  # - template: Security/Container-Scanning.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  # - template: Security/Dependency-Scanning.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  # - template: Security/License-Management.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/License-Management.gitlab-ci.yml
  # - template: Security/SAST.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
